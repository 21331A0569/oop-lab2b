//Demonstrate method/function overlaoding in Java.
class Sample{
    private void add(int a){
        System.out.println("Method with one param a+2="+(a+2));
    }
    protected void add(int a,int b){
        System.out.println("Method with two param a+b="+(a+b));
        add(10);//we are calling the private method 
    }
    public void add(int a,int b,int c){
        System.out.println("Method with three param a+b+c="+(a+b+c));
    }
}
public class MethodOl{
    public static void main(String[] args) {
        Sample obj=new Sample();
        //obj.add(10); //private method can not accessible
        obj.add(10, 20);//protected method
        obj.add(10,20,30);//public method
    } 
}