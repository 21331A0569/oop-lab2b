import cv2
import face_recognition
import tkinter as tk
from tkinter import ttk, messagebox
import csv
import datetime
import os

# Create a GUI window
root = tk.Tk()
root.title("Face Recognition Attendance System")

# Function to handle window close event
def on_closing():
    if messagebox.askokcancel("Quit", "Do you want to quit?"):
        root.destroy()

root.protocol("WM_DELETE_WINDOW", on_closing)

# Function to open the registration window
def open_registration_window():
    registration_window = tk.Toplevel(root)
    registration_window.title("Registration")

    student_id_label = tk.Label(registration_window, text="Student ID:")
    student_id_label.pack()
    student_id_entry = tk.Entry(registration_window)
    student_id_entry.pack()

    student_name_label = tk.Label(registration_window, text="Student Name:")
    student_name_label.pack()
    student_name_entry = tk.Entry(registration_window)
    student_name_entry.pack()

    def capture_image():
        student_id = student_id_entry.get()
        student_name = student_name_entry.get()

        if not student_id or not student_name:
            messagebox.showerror("Error", "Please enter both Student ID and Student Name.")
            return

        # Capture an image from the webcam
        cap = cv2.VideoCapture(0)
        ret, frame = cap.read()
        cap.release()

        # Save the captured image with the student's ID as the filename
        image_filename = f"{student_id}.jpg"
        cv2.imwrite(image_filename, frame)

        # Append the student data to the CSV file
        with open("students.csv", "a", newline="") as csvfile:
            csvwriter = csv.writer(csvfile)
            csvwriter.writerow([student_id, student_name, image_filename])

        messagebox.showinfo("Registration", f"Student {student_name} registered successfully!")

    capture_button = tk.Button(registration_window, text="Capture Image", command=capture_image)
    capture_button.pack()

# Create a button for registration
button_register = tk.Button(root, text="Register Student", command=open_registration_window)
button_register.pack()

root.mainloop()

