#include <iostream>
using namespace std;
class Sample {
private:
    int num1,num2; 
public:
    Sample(){
        num1 =0;
        num2=0;
    }
    Sample(int a, int b)
    {
        num1=a;
        num2=b;
    }
    Sample operator*(Sample &obj1)
    {
        Sample obj3;
        obj3.num1=obj1.num1*num1;
        obj3.num2=obj1.num2*num2;
        return obj3;
    }
    void display(){
        cout<<num1<<" and "<<num2<<endl;
    }
};
 
int main()
{
    Sample s1(5,6);
    Sample s2(7,8);
    Sample s3=s1*s2;
    s1.display();
    s2.display();
    s3.display();
}