import java.util.HashMap;
import java.util.Random;
import java.util.Arrays;
class Observation{
    public static void main(String[] args) {
        Random obj=new Random();
        HashMap<String,String> pairs=new HashMap<String,String>();
        String [] studentValues={"a3","a4","a5","a6","a7","a8","a9","b0","b1","b2","b3","b4",
                            "b5","b6","b7","b8","b9","c0","c1","c2","c3","c4","c5","c6","c7",
                            "c8","c9","d0","d1","d2","l7","l8","l9","l10","l11","l12"};
        String  studentKeys[]=new String[36];//={"a0","a1","a2"};
        int start=67;
        for(int i=0;i<=32;i++){
            studentKeys[i]=Integer.toString(start);
            start++;
        } 
        studentKeys[33]="a0";
        studentKeys[34]="a1";
        studentKeys[35]="a2";
        Integer [] indexValues=new Integer[36];
        for(int i=0;i<36;i++){
            int index=obj.nextInt(36);
            while(Arrays.asList(indexValues).contains(index)){
                 index=obj.nextInt(36);
            }
            pairs.put(studentKeys[i],studentValues[index]);
            indexValues[i]=index;
        }
        System.out.println(pairs);
    }
}