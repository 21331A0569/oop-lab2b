import cv2
import face_recognition
import tkinter as tk
from tkinter import messagebox
import csv
import os
import datetime

# Create a GUI window
root = tk.Tk()
root.title("Face Recognition System")

# Function to handle window close event
def on_closing():
    if messagebox.askokcancel("Quit", "Do you want to quit?"):
        root.destroy()

root.protocol("WM_DELETE_WINDOW", on_closing)

# Function to open the recognition window
def open_recognition_window():
    recognition_window = tk.Toplevel(root)
    recognition_window.title("Recognition")

    # Function to handle window close event in the recognition window
    def on_recognition_window_closing():
        if messagebox.askokcancel("Close Recognition", "Do you want to close recognition?"):
            recognition_window.destroy()

    recognition_window.protocol("WM_DELETE_WINDOW", on_recognition_window_closing)

    # Load registered student data from the CSV file
    registered_students = []
    with open("students.csv", "r") as csvfile:
        csvreader = csv.reader(csvfile)
        for row in csvreader:
            student_id, student_name, student_image_filename = row
            registered_students.append((student_id, student_name, student_image_filename))

    # Initialize video capture from the webcam
    cap = cv2.VideoCapture(1)

    # Initialize attendance log
    attendance_log = set()  # Use a set to store unique entries

    while True:
        ret, frame = cap.read()

        if not ret:
            break

        # Find face locations in the frame
        face_locations = face_recognition.face_locations(frame)
        face_encodings = face_recognition.face_encodings(frame, face_locations)

        # Match detected faces with registered students
        for face_encoding, (top, right, bottom, left) in zip(face_encodings, face_locations):
            for student_id, student_name, student_image_filename in registered_students:
                # Load the registered student's face encoding
                registered_face_encoding = face_recognition.face_encodings(
                    face_recognition.load_image_file(student_image_filename)
                )[0]

                # Compare the detected face encoding with the registered face encoding
                is_match = face_recognition.compare_faces([registered_face_encoding], face_encoding)[0]

                if is_match:
                    # Create a unique entry for attendance
                    current_date_time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
                    attendance_entry = (student_id, student_name, current_date_time)

                    if attendance_entry not in attendance_log:
                        # Display student information on the video feed
                        cv2.rectangle(frame, (left, top), (right, bottom), (0, 255, 0), 2)
                        label = f"ID: {student_id}, Name: {student_name}, Time: {current_date_time}"
                        cv2.putText(frame, label, (left, top - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255), 2)

                        # Log attendance
                        attendance_log.add(attendance_entry)

        cv2.imshow("Face Recognition", frame)

        if cv2.waitKey(1) & 0xFF == ord("q"):
            break

    cap.release()
    cv2.destroyAllWindows()

    # Update attendance CSV file
    with open("attendance.csv", "a") as csvfile:
        csvwriter = csv.writer(csvfile)
        csvwriter.writerows(attendance_log)

# Create a button for recognition
button_recognize = tk.Button(root, text="Recognize", command=open_recognition_window)
button_recognize.pack()

root.mainloop()

